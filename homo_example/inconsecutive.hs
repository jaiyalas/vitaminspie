-- {{}}
-- {{},{0}}
-- {{},{0},{1}}
-- {{},{0},{1},{2},{0,2}}

inc :: Int -> [[Int]]
inc 0 = [[]]
inc 1 = [[],[0]]
inc n = inc (n-1) ++ map (++[n]) (inc (n-2))

-- The problem is, how to construct this as a list homo?1