module Main where
  import Control.Monad
  import System.Cmd
  import System.Environment

  help = "wrong parameter!!\nPlease enter:\n \t> push bitbucket, or\n \t> push github"

  main = do
    args <- getArgs
    case args of
      ["bitbucket"] -> pushBitbucket
      ["bit"]       -> pushBitbucket
      ["bb"]        -> pushBitbucket
      ["origin"]    -> pushBitbucket
      ["github"]    -> pushGithub
      ["git"]       -> pushGithub
      ["gh"]        -> pushGithub
      otherwise     -> do
        putStrLn help
        
  pushGithub = do
    out <- rawSystem "git" ["push","-u","github","master","release"]
    putStrLn $ ("PROGRAM EXIT WITH STATE: "++) $ show out

  pushBitbucket = do
    out <- rawSystem "git" ["push","-u","origin","--all"]
    putStrLn $ ("PROGRAM EXIT WITH STATE: "++) $ show out

